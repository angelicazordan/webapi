const elEntrada = document.querySelector('#entrada');
const elButton = document.querySelector('button');
const elLista = document.querySelector('#lista');

elEntrada.focus()

const addItem = (lista, entrada) => {
    const texto = entrada.value;
    if (texto) lista.innerHTML = `<li>${texto}<button>Remover</button></li>` + lista.innerHTML;
    entrada.value = ""   
    elEntrada.focus()
}

const addByClick = evt =>{
    evt.preventDefault();
    addItem(elLista, elEntrada)
}

const addByEnter = evt =>{
    if(evt.keyCode === 13){
        evt.preventDefault()
        addItem(elLista, elEntrada)
    }
}

const removeItem = evt => {
    if (evt.target.nodeName === "BUTTON"){
        const item = evt.target.parentNode
        item.remove()
    }
    elEntrada.focus()
}

elButton.addEventListener("click", addByClick)
elEntrada.addEventListener("keyPressed", addByEnter)
elLista.addEventListener("click", removeItem)

