

function listarProdutos(){

    let loja = JSON.parse(localStorage.getItem("PLoja"))
    let carrinho = loja.carrinho

    document.writeln('<form name="formItens"')
    document.writeln('<div>')
    document.writeln('<table whidth="750" cellpadding="3">')
    document.writeln('<tr><td>PRODUTO</b></td><td>QUANTIDADE</b></td><td>VALOR POR UNIDADE</td><td>VALOR TOTAL</td><td>&nbsp;</td></tr>');
    document.writeln('<tr> <td colspan></td></tr>')
    const totalGeral = geraItens(carrinho.produtos)
    document.writeln("</table>")
    document.writeln("</div>")
    document.writeln("</form>")

    document.writeln('<div>')
    document.writeln('<table whidth="750" cellpadding="3">')
    document.writeln('<tr> <td><p>TOTAL GERAL DA SUA COMPRA</p></td> <td><p id="totalGeral">R$ ' + totalGeral+ ' </p></td> </tr>')
    document.writeln('</table>')
    document.writeln('</div>')
}

function geraItens(arrayProdutos){
    let totalGeral = 0

    if(arrayProdutos){

        
        arrayProdutos.forEach((item) => {
            
            const totalItem = item.valorProduto * item.qtdeProduto

            totalGeral = totalGeral + totalItem

            document.writeln('<tr>')
            document.writeln('<td>' + item.nomeProduto + '</td>')
            document.writeln('<td>' + item.qtdeProduto + '</td>')
            document.writeln('<td>'+ item.valorProduto+'</td>')
            document.writeln('<td>'+ totalItem + '</td>')
            document.writeln('</tr>')
            
        })

    }

    return totalGeral
}

function finalizarCompra(){

    let loja = JSON.parse(localStorage.getItem("PLoja"))
    let compras = loja.comprasFinalizadas


    if (confirm("Deseja finalizar a sua compra?")){
        const carrinho = JSON.parse(localStorage.getItem("PCarrinho"))

        const totalGeral = document.querySelector('#totalGeral')
        const nome = document.querySelector('#txtNome')
        const email = document.querySelector('#txtEmail')
        const telefone = document.querySelector('#txtTelefone')
        const endereco = document.querySelector('#txtEndereco')
        const bairro = document.querySelector('#txtBairro')
        const cidade = document.querySelector('#txtCidade')
        const cep = document.querySelector('#txtCEP')
        const formaPgto = document.querySelector('#pagamento')
        const comentarios = document.querySelector('#comentarios')
        
        const novaCompra = { produtos: carrinho.produtos,
            dataCompra: new Date(),
            totalCompra: totalGeral.textContent,
            nome: nome.value,
            email: email.value,
            telefone: telefone.value,
            endereco: endereco.value,
            bairro: bairro.value,
            cidade: cidade.value,
            cep: cep.value,
            formaPgto: formaPgto.value,
            comentarios: comentarios.value
        }

        compras.push(novaCompra)

        loja.comprasFinalizadas = compras

        console.log("compra adicionada", compras)

        localStorage.setItem("PLoja", JSON.stringify(loja))  

    
        limparCarrinho()

        testaStorage()

        alert("Compra finalizada com sucesso!!")

    }    
}

function limparCarrinho(){
    let loja = JSON.parse(localStorage.getItem("PLoja"))
    let carrinho = {produtos: []} 
    loja.carrinho = carrinho
    localStorage.setItem("PLoja", JSON.stringify(loja))
}

function testaStorage(){

    console.log("compras", JSON.parse(localStorage.getItem("PLoja")))

}
