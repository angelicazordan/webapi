window.defaultStatus = "Compras online"

let loja 
let carrinho

inicializa()

function inicializa(){
    loja = JSON.parse(localStorage.getItem("PLoja"))

    if (!loja){
        loja = { carrinho:{ produtos: []},
                comprasFinalizadas: []
            }
    }

    carrinho = loja.carrinho

}

function comprarItem(produto, valor, quantidade){
    console.log("comprando o item")

    if(quantidade <= 0){
        alert("A quantidade do produto deve ser maior que 1. Verifique!")
    }else{
        if (confirm("Deseja adicionar " + quantidade + "X " + produto + "ao seu carrinho?")){
            let index = document.cookie.indexOf("PCarrinho")
            let countIni = (document.cookie.indexOf("=", index) + 1)
            let countFim = document.cookie.indexOf(";", index)

            if (countFim == -1){
                countFim = document.cookie.length
            }

            const carrinhoExistente = document.cookie.substring(countIni, countFim)
            const produtoAdd = "[" + produto + "|" + valor + "|" + quantidade + "]"
            
            console.log("Produto add", produtoAdd)
            document.cookie = "PCarrinho="+ carrinhoExistente + produtoAdd

        }
    }
}

function comprarItem2(produto, valor, quantidade){
    console.log("iniciando compra 2")
    
    if (quantidade <= 0){
        alert("A quantidade do produto deve ser maior que 1. Verifique!")
    }else{
        if (confirm("Deseja adicionar " + quantidade + "X " + produto + " ao seu carrinho?")){
            
            console.log("carrinho", carrinho)

            
            
            const novoitem = {
                nomeProduto: produto,
                valorProduto: valor,
                qtdeProduto: quantidade
            }

            let itemExistente = carrinho.produtos.findIndex( item => item.nomeProduto === novoitem.nomeProduto)

            
            if (itemExistente < 0){
                carrinho.produtos.push(novoitem)
            }else{

                carrinho.produtos[itemExistente].qtdeProduto =  parseInt(carrinho.produtos[itemExistente].qtdeProduto) + parseInt(quantidade)
            }

            console.log(carrinho)

            loja.carrinho = carrinho

            console.log(loja)


            localStorage.setItem("PLoja", JSON.stringify(loja))
        }
    }

}

function resetaCarrinho(){
    let index = document.cookie.indexOf("PCarrinho")
    document.cookie = "PCarrinho=."

    if (!document.cookie){
        alert("Esta loja faz a utilização de cookies!\n Mantenha-os ativados em seu navegador.")
    }
}